import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import ContactForm from "../components/ContactForm/ContactForm";

describe("ContactForm Component", () => {
  it("renders contact form with initial state", () => {
    const { getByText, getByLabelText } = render(<ContactForm />);
    expect(getByText("Contacto")).toBeInTheDocument();
    expect(getByLabelText("Nombre")).toBeInTheDocument();
    expect(getByLabelText("Email")).toBeInTheDocument();
    expect(getByLabelText("Mensaje")).toBeInTheDocument();
    expect(getByText("Enviar")).toBeInTheDocument();
  });

  it("submits the form with user input", async () => {
    const { getByLabelText, getByText } = render(<ContactForm />);

    fireEvent.change(getByLabelText("Nombre"), {
      target: { value: "John Doe" },
    });
    fireEvent.change(getByLabelText("Email"), {
      target: { value: "john.doe@example.com" },
    });
    fireEvent.change(getByLabelText("Mensaje"), {
      target: { value: "Hola, este es un mensaje de prueba." },
    });

    fireEvent.click(getByText("Enviar"));

    await waitFor(() => {
      expect(getByText("¡Gracias por tu mensaje!")).toBeInTheDocument();
    });
  });
});
