import { render } from "@testing-library/react";
import Header from "../components/Header/Header";

describe("Header Component", () => {
  it("renders header with correct text", () => {
    const { getByText } = render(<Header />);
    const headerElement = getByText(/Heyner Humanez Almanza/i);
    expect(headerElement).toBeInTheDocument();
  });

  it("renders profile icon", () => {
    const { container } = render(<Header />);
    const profileSvg = container.querySelector("svg");
    expect(profileSvg).toBeInTheDocument();
  });

  it("applies CSS class correctly", () => {
    const { container } = render(<Header />);
    const headerElement = container.querySelector("header");

    expect(headerElement).toHaveClass("header");
  });
});
