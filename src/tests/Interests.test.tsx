import React from "react";
import { render } from "@testing-library/react";
import Interests from "../components/Interests/Interests";

describe("Interests component", () => {
  it("renders interests correctly", () => {
    const { getByText } = render(<Interests />);

    expect(getByText("Intereses")).toBeInTheDocument();

    const interests = [
      "Programación",
      "Inteligencia Artificial",
      "Eventos de música en vivo",
      "Vida saludable",
      "Emprendimiento",
    ];

    interests.forEach((interest) => {
      expect(getByText(interest)).toBeInTheDocument();
    });
  });

  it("applies CSS class correctly", () => {
    const { container } = render(<Interests />);
    const interestItems = container.querySelectorAll("li");

    interestItems.forEach((item, index) => {
      const expectedColorClass = `color${index + 1}`;
      expect(item).toHaveClass(expectedColorClass);
      expect(item).toHaveClass("animate__animated");
      expect(item).toHaveClass("animate__bounce");
      expect(item).toHaveClass("animate__delay-5s");
    });
  });
});
