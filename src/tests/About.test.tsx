import React from "react";
import { render } from "@testing-library/react";
import About from "../components/About/About";

describe("About Component", () => {
  it("renders about section with correct content", () => {
    const { getByText } = render(<About />);

    expect(getByText("Sobre mí")).toBeInTheDocument();
    expect(getByText(/Heyner Humanez/i)).toBeInTheDocument();
    expect(getByText(/desarrollador web calificado/i)).toBeInTheDocument();
    expect(getByText(/pasión a su próximo proyecto/i)).toBeInTheDocument();
  });

  it("applies CSS class correctly", () => {
    const { container } = render(<About />);
    const sectionElement = container.querySelector("section");

    expect(sectionElement).toHaveClass("about");
  });
});
