import styles from "./Header.module.css";
import { Profile } from "../Icons/Icons";

const Header: React.FC = () => {
  return (
    <header className={styles.header}>
      <Profile />
      <h1 className="animate__animated animate__fadeIn">
        Heyner Humanez Almanza
      </h1>
    </header>
  );
};

export default Header;
