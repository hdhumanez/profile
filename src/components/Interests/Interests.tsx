import styles from "./Interests.module.css";

const Interests: React.FC = () => {
  const interests = [
    { id: 1, text: "Programación", colorClass: styles.color1 },
    { id: 2, text: "Inteligencia Artificial", colorClass: styles.color2 },
    { id: 3, text: "Eventos de música en vivo", colorClass: styles.color3 },
    { id: 4, text: "Vida saludable", colorClass: styles.color4 },
    { id: 5, text: "Emprendimiento", colorClass: styles.color5 },
  ];

  return (
    <section className={styles.interests}>
      <h2>Intereses</h2>
      <ul>
        {interests.map((interest) => (
          <li
            key={interest.id}
            className={`${interest.colorClass} animate__animated animate__bounce animate__delay-5s`}
          >
            {interest.text}
          </li>
        ))}
      </ul>
    </section>
  );
};

export default Interests;
