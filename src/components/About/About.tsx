import styles from "./About.module.css";

const About: React.FC = () => {
  return (
    <section className={styles.about}>
      <h2>Sobre mí</h2>
      <p className="animate__animated animate__fadeIn animate__delay-2s">
        Hola, soy Heyner Humanez, un desarrollador web calificado y profesional
        con 1 año y 6 meses meses de experiencia en administración de bases de
        datos, REST API y diseño de sitios web. Tengo habilidades creativas y
        analíticas sólidas, así como experiencia en mantenimiento preventivo de
        computadoras. ¡No le tengo miedo a los desafíos!
      </p>
      <p className="animate__animated animate__fadeIn animate__delay-3s">
        Creo que el diseño es algo más que hacer que las cosas luzcan bonitas:
        se trata de resolver problemas y crear experiencias intuitivas y
        agradables para los usuarios.
      </p>
      <p className="animate__animated animate__fadeIn animate__delay-5s">
        Ya sea que esté trabajando en un sitio web, una aplicación móvil u otro
        producto digital, aporto mi compromiso con la excelencia en el diseño y
        el pensamiento centrado en el usuario a cada proyecto en el que trabajo.
        Espero tener la oportunidad de aportar mis habilidades y pasión a su
        próximo proyecto.
      </p>
    </section>
  );
};

export default About;
