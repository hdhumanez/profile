# Pasos para ejecutar el proyecto

- Clonar el repositorio https://gitlab.com/hdhumanez/profile.git
- Instalar pnpm https://pnpm.io/es/installation

## Config. inicial

Para desarrollo

```bash
  pnpm i
  pnpm run dev
```

hay un script adicional para hacer pre-commit con husky, esto con el fin de impedir hacer push a ramas si tienen errores de linter con eslint o falla el testing con jest. lo que obliga a desarrolladores a seguir las reglas de linter y testing.

```bash
  pnpm i
  pnpm run prepare

```

Al activar prepare se activa husky junto las reglas de eslint y jest.

Para producción

```bash
  pnpm i
  pnpm run build
  pnpm start
```

## Authors

- [@hdhumanez](https://www.gitlab.com/hdhumanez)
